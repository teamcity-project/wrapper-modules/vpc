# VPC wrapper module for Teamcity project

This module is just wrapper for great [terraform-aws-vpc](https://github.com/terraform-aws-modules/terraform-aws-vpc) module with some subnet presets:

* Example of public subnets for network 10.100.0.0/16:
  - 10.100.11.0/24
  - 10.100.21.0/24
  - 10.100.31.0/24

* Example of private subnets for network 10.100.0.0/16:
  - 10.100.12.0/24
  - 10.100.22.0/24
  - 10.100.32.0/24

* Example of intra subnets for network 10.100.0.0/16:
  - 10.100.13.0/24
  - 10.100.23.0/24
  - 10.100.33.0/24 

## Requirements
Terraform  0.12

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Providers

| Name | Version |
|------|---------|
| aws | ~> 2.23 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| azs\_count | Amount of availability zones in the region where subnets should be created | `number` | n/a | yes |
| cidr | The CIDR block for the VPC | `string` | n/a | yes |
| create\_database\_subnet\_group | Controls if database subnet group should be created | `bool` | `false` | no |
| dhcp\_options\_domain\_name | Specifies DNS name for DHCP options set (requires enable\_dhcp\_options set to true) | `string` | `""` | no |
| dhcp\_options\_domain\_name\_servers | Specify a list of DNS server addresses for DHCP options set, default to AWS provided (requires enable\_dhcp\_options set to true) | `list(string)` | <pre>[<br>  "AmazonProvidedDNS"<br>]</pre> | no |
| enable\_dhcp\_options | Should be true if you want to specify a DHCP options set with a custom domain name, DNS servers, NTP servers, netbios servers, and/or netbios server type | `bool` | `false` | no |
| enable\_dns\_hostnames | Should be true to enable DNS hostnames in the VPC | `bool` | `true` | no |
| enable\_dns\_support | Should be true to enable DNS support in the VPC | `bool` | `true` | no |
| enable\_nat\_gateway | Should be true if you want to provision NAT Gateways for each of your private networks | `bool` | `false` | no |
| environment | Environment name | `string` | `""` | no |
| one\_nat\_gateway\_per\_az | Should be true if you want only one NAT Gateway per availability zone. Requires `var.azs` to be set, and the number of `public_subnets` created to be greater than or equal to the number of availability zones specified in `var.azs`. | `bool` | `false` | no |
| region | AWS region | `string` | n/a | yes |
| single\_nat\_gateway | Should be true if you want to provision a single shared NAT Gateway across all of your private networks | `bool` | `false` | no |
| suffix | Suffix in VPC name | `string` | `""` | no |
| tags | A map of tags to add to all resources | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| azs | A list of availability zones specified as argument to this module |
| default\_security\_group\_id | The ID of the security group created by default on VPC creation |
| intra\_subnets | List of IDs of intra subnets |
| intra\_subnets\_cidr\_blocks | List of cidr\_blocks of intra subnets |
| private\_subnets | List of IDs of private subnets |
| private\_subnets\_cidr\_blocks | List of cidr\_blocks of private subnets |
| public\_subnets | List of IDs of public subnets |
| public\_subnets\_cidr\_blocks | List of cidr\_blocks of public subnets |
| vpc\_id | The ID of the VPC |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
