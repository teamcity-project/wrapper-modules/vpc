terraform {
  # The configuration for this backend will be filled in by Terragrunt or via a backend.hcl file. See
  # https://www.terraform.io/docs/backends/config.html#partial-configuration
  backend "s3" {}

  # Only allow this Terraform version. Note that if you upgrade to a newer version, Terraform won't allow you to use an
  # older version, so when you upgrade, you should upgrade everyone on your team and your CI servers all at once.
  required_version = "~> 0.12.6"

  required_providers {
    aws = "~> 2.23"
  }
}

provider "aws" {
  region = var.region
}

locals {
  # Example of public subnets for network 10.100.0.0/16:
  # - 10.100.11.0/24
  # - 10.100.21.0/24
  # - 10.100.31.0/24

  public_subnets = [
    for num in range(1, var.azs_count + 1) :
    cidrsubnet(var.cidr, 8, 10 * num + 1)
  ]

  # Example of private subnets for network 10.100.0.0/16:
  # - 10.100.12.0/24
  # - 10.100.22.0/24
  # - 10.100.32.0/24

  private_subnets = [
    for num in range(1, var.azs_count + 1) :
    cidrsubnet(var.cidr, 8, 10 * num + 2)
  ]

  # Example of intra subnets for network 10.100.0.0/16:
  # - 10.100.13.0/24
  # - 10.100.23.0/24
  # - 10.100.33.0/24

  intra_subnets = [
    for num in range(1, var.azs_count + 1) :
    cidrsubnet(var.cidr, 8, 10 * num + 3)
  ]

  suffix = var.suffix == "" ? "" : "-${var.suffix}"
}

data "aws_availability_zones" "available" {}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "~> v2.0"

  name = "${var.environment}-vpc${local.suffix}"
  cidr = var.cidr
  azs  = slice(sort(data.aws_availability_zones.available.names), 0, var.azs_count)

  private_subnets = local.private_subnets
  public_subnets  = local.public_subnets
  intra_subnets   = local.intra_subnets

  create_database_subnet_group = var.create_database_subnet_group

  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support   = var.enable_dns_support

  enable_nat_gateway     = var.enable_nat_gateway
  single_nat_gateway     = var.single_nat_gateway
  one_nat_gateway_per_az = var.one_nat_gateway_per_az

  enable_dhcp_options              = var.enable_dhcp_options
  dhcp_options_domain_name         = var.dhcp_options_domain_name
  dhcp_options_domain_name_servers = var.dhcp_options_domain_name_servers

  tags = merge(var.tags, map("Environment", format("%s", var.environment)))
}
